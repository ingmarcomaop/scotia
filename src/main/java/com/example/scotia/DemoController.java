package com.example.scotia;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
    @RequestMapping(value = "welcome")
    public String hello(){
        return "Bienvenidos a la conferencia DevSecOps por Scotia Bank.";
    }
}
